#pragma once

#include <string>
#include <vector>

class Options
{
public:
	bool help;
	bool version;
	bool print;
	std::string error;
	std::string remote;
	std::string branch;
	bool issues;
	bool new_issue;
	bool issue_from_branch;
	bool pull_requests;
	bool new_pr;
	bool pipelines;
	bool actions;

	Options();

	bool parse_command_line(std::vector<std::string> args);
	int get_issue_nr_from_branch();
};
