#include <catch2/catch.hpp>
#include "../remote.h"

TEST_CASE("Remote::set_from_url(), github.com, https")
{
	Remote rmt(nullptr);
	const auto res = rmt.set_from_url("https://github.com/user/repo.git");
	REQUIRE(res == true);
	REQUIRE(rmt.url() == "https://github.com/user/repo");
	REQUIRE(rmt.host == Remote::github);
}

TEST_CASE("Remote::set_from_url(), github.com, scp-notation")
{
	Remote rmt(nullptr);
	const auto res = rmt.set_from_url("git@github.com:user/repo.git");
	REQUIRE(res == true);
	REQUIRE(rmt.url() == "https://github.com/user/repo");
	REQUIRE(rmt.host == Remote::github);
}

TEST_CASE("Remote::set_from_url(), github.com, ssh-url")
{
	Remote rmt(nullptr);
	const auto res = rmt.set_from_url("ssh://git@github.com/user/repo.git");
	REQUIRE(res == true);
	REQUIRE(rmt.url() == "https://github.com/user/repo");
	REQUIRE(rmt.host == Remote::github);
}

TEST_CASE("Remote::set_from_url(), github.com, git-url")
{
	Remote rmt(nullptr);
	const auto res = rmt.set_from_url("git://github.com/user/repo.git");
	REQUIRE(res == true);
	REQUIRE(rmt.url() == "https://github.com/user/repo");
	REQUIRE(rmt.host == Remote::github);
}
