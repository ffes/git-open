#include <catch2/catch.hpp>
#include "../remote.h"
#include "../options.h"

TEST_CASE("Remote::url(), github")
{
	Options options;

	Remote rmt(&options);
	rmt.set_from_url("https://github.com/user/repo.git");

	REQUIRE(rmt.url() == "https://github.com/user/repo");
	REQUIRE(rmt.host == Remote::github);
}

TEST_CASE("Remote::url(), github, issues")
{
	Options options;
	options.issues = true;

	Remote rmt(&options);
	rmt.set_from_url("https://github.com/user/repo.git");

	REQUIRE(rmt.url() == "https://github.com/user/repo/issues");
	REQUIRE(rmt.host == Remote::github);
}

TEST_CASE("Remote::url(), github, new_issue")
{
	Options options;
	options.new_issue = true;

	Remote rmt(&options);
	rmt.set_from_url("https://github.com/user/repo.git");

	REQUIRE(rmt.url() == "https://github.com/user/repo/issues/new");
	REQUIRE(rmt.host == Remote::github);
}

TEST_CASE("Remote::url(), github, issue_from_branch")
{
	Options options;
	options.issue_from_branch = true;
	options.branch = "fix-23";

	Remote rmt(&options);
	rmt.set_from_url("https://github.com/user/repo.git");

	REQUIRE(rmt.url() == "https://github.com/user/repo/issues/23");
	REQUIRE(rmt.host == Remote::github);
}

TEST_CASE("Remote::url(), github, issue_from_branch, invalid branch")
{
	Options options;
	options.issue_from_branch = true;
	options.branch = "some-branch-name";

	Remote rmt(&options);
	rmt.set_from_url("https://github.com/user/repo.git");

	REQUIRE(rmt.url() == "https://github.com/user/repo/issues");
	REQUIRE(rmt.host == Remote::github);
}

TEST_CASE("Remote::url(), github, pull_requests")
{
	Options options;
	options.pull_requests = true;

	Remote rmt(&options);
	rmt.set_from_url("https://github.com/user/repo.git");

	REQUIRE(rmt.url() == "https://github.com/user/repo/pulls");
	REQUIRE(rmt.host == Remote::github);
}

TEST_CASE("Remote::url(), github, new-pr, branch not set")
{
	Options options;
	options.new_pr = true;
	options.branch = "";

	Remote rmt(&options);
	rmt.set_from_url("https://github.com/user/repo.git");

	REQUIRE(rmt.url() == "https://github.com/user/repo/compare");
	REQUIRE(rmt.host == Remote::github);
}

TEST_CASE("Remote::url(), gitlab, new-nr, branch = pr-branch")
{
	Options options;
	options.new_pr = true;
	options.branch = "pr-branch";

	Remote rmt(&options);
	rmt.set_from_url("https://github.com/user/repo.git");

	REQUIRE(rmt.url() == "https://github.com/user/repo/pull/new/pr-branch");
	REQUIRE(rmt.host == Remote::github);
}

TEST_CASE("Remote::url(), github, actions")
{
	Options options;
	options.actions = true;

	Remote rmt(&options);
	rmt.set_from_url("https://github.com/user/repo.git");

	REQUIRE(rmt.url() == "https://github.com/user/repo/actions");
	REQUIRE(rmt.host == Remote::github);
}

TEST_CASE("Remote::url(), github, pipelines")
{
	Options options;
	options.pipelines = true;

	Remote rmt(&options);
	rmt.set_from_url("https://github.com/user/repo.git");

	REQUIRE(rmt.url() == "https://github.com/user/repo/actions");
	REQUIRE(rmt.host == Remote::github);
}
