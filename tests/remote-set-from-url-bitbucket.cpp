#include <catch2/catch.hpp>
#include "../remote.h"

TEST_CASE("Remote::set_from_url(), bitbucket.org, https")
{
	Remote rmt(nullptr);
	const auto res = rmt.set_from_url("https://bitbucket.org/user/repo.git");
	REQUIRE(res == true);
	REQUIRE(rmt.url() == "https://bitbucket.org/user/repo");
	REQUIRE(rmt.host == Remote::bitbucket);
}

TEST_CASE("Remote::set_from_url(), bitbucket.org, scp-notation")
{
	Remote rmt(nullptr);
	const auto res = rmt.set_from_url("git@bitbucket.org:user/repo.git");
	REQUIRE(res == true);
	REQUIRE(rmt.url() == "https://bitbucket.org/user/repo");
	REQUIRE(rmt.host == Remote::bitbucket);
}
