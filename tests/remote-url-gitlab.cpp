#include <catch2/catch.hpp>
#include "../remote.h"
#include "../options.h"

TEST_CASE("Remote::url(), gitlab")
{
	Options options;

	Remote rmt(&options);
	rmt.set_from_url("https://gitlab.com/user/repo.git");

	REQUIRE(rmt.url() == "https://gitlab.com/user/repo");
	REQUIRE(rmt.host == Remote::gitlab);
}

TEST_CASE("Remote::url(), gitlab, issues")
{
	Options options;
	options.issues = true;

	Remote rmt(&options);
	rmt.set_from_url("https://gitlab.com/user/repo.git");

	REQUIRE(rmt.url() == "https://gitlab.com/user/repo/-/issues");
	REQUIRE(rmt.host == Remote::gitlab);
}

TEST_CASE("Remote::url(), gitlab, new_issue")
{
	Options options;
	options.new_issue = true;

	Remote rmt(&options);
	rmt.set_from_url("https://gitlab.com/user/repo.git");

	REQUIRE(rmt.url() == "https://gitlab.com/user/repo/-/issues/new");
	REQUIRE(rmt.host == Remote::gitlab);
}

TEST_CASE("Remote::url(), gitlab, issue_from_branch")
{
	Options options;
	options.issue_from_branch = true;
	options.branch = "fix-23";

	Remote rmt(&options);
	rmt.set_from_url("https://gitlab.com/user/repo.git");

	REQUIRE(rmt.url() == "https://gitlab.com/user/repo/-/issues/23");
	REQUIRE(rmt.host == Remote::gitlab);
}

TEST_CASE("Remote::url(), gitlab, issue_from_branch, invalid branch")
{
	Options options;
	options.issue_from_branch = true;
	options.branch = "some-branch-name";

	Remote rmt(&options);
	rmt.set_from_url("https://gitlab.com/user/repo.git");

	REQUIRE(rmt.url() == "https://gitlab.com/user/repo/-/issues");
	REQUIRE(rmt.host == Remote::gitlab);
}

TEST_CASE("Remote::url(), gitlab, pull_requests")
{
	Options options;
	options.pull_requests = true;

	Remote rmt(&options);
	rmt.set_from_url("https://gitlab.com/user/repo.git");

	REQUIRE(rmt.url() == "https://gitlab.com/user/repo/-/merge_requests");
	REQUIRE(rmt.host == Remote::gitlab);
}

TEST_CASE("Remote::url(), gitlab, new-mr, branch not set")
{
	Options options;
	options.new_pr = true;
	options.branch = "";

	Remote rmt(&options);
	rmt.set_from_url("https://gitlab.com/user/repo.git");

	REQUIRE(rmt.url() == "https://gitlab.com/user/repo/-/merge_requests/new");
	REQUIRE(rmt.host == Remote::gitlab);
}

TEST_CASE("Remote::url(), gitlab, new-mr, branch = mr-branch")
{
	Options options;
	options.new_pr = true;
	options.branch = "mr-branch";

	Remote rmt(&options);
	rmt.set_from_url("https://gitlab.com/user/repo.git");

	REQUIRE(rmt.url() == "https://gitlab.com/user/repo/-/merge_requests/new?merge_request%5Bsource_branch%5D=mr-branch");
	REQUIRE(rmt.host == Remote::gitlab);
}

TEST_CASE("Remote::url(), gitlab, pipelines")
{
	Options options;
	options.pipelines = true;

	Remote rmt(&options);
	rmt.set_from_url("https://gitlab.com/user/repo.git");

	REQUIRE(rmt.url() == "https://gitlab.com/user/repo/-/pipelines");
	REQUIRE(rmt.host == Remote::gitlab);
}

TEST_CASE("Remote::url(), gitlab, actions")
{
	Options options;
	options.actions = true;

	Remote rmt(&options);
	rmt.set_from_url("https://gitlab.com/user/repo.git");

	REQUIRE(rmt.url() == "https://gitlab.com/user/repo/-/pipelines");
	REQUIRE(rmt.host == Remote::gitlab);
}
