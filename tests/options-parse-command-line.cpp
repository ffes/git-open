#include <catch2/catch.hpp>
#include <string>
#include <vector>
#include "../options.h"

TEST_CASE("Options::parse_command_line(), -h")
{
	std::vector<std::string> args;
	args.push_back("-h");

	Options opt;
	const auto res = opt.parse_command_line(args);

	REQUIRE(res == true);
	REQUIRE(opt.help == true);
}

TEST_CASE("Options::parse_command_line(), --print")
{
	std::vector<std::string> args;
	args.push_back("--print");

	Options opt;
	const auto res = opt.parse_command_line(args);

	REQUIRE(res == true);
	REQUIRE(opt.print == true);
}

TEST_CASE("Options::parse_command_line(), --version")
{
	std::vector<std::string> args;
	args.push_back("--version");

	Options opt;
	const auto res = opt.parse_command_line(args);

	REQUIRE(res == true);
	REQUIRE(opt.version == true);
}

TEST_CASE("Options::parse_command_line(), --invalid-option")
{
	std::vector<std::string> args;
	args.push_back("--invalid-option");

	Options opt;
	const auto res = opt.parse_command_line(args);

	REQUIRE(res == false);
	REQUIRE(opt.error == "Invald option: --invalid-option");
}

TEST_CASE("Options::parse_command_line(), --issues")
{
	std::vector<std::string> args;
	args.push_back("--issues");

	Options opt;
	const auto res = opt.parse_command_line(args);

	REQUIRE(res == true);
	REQUIRE(opt.issues == true);
}

TEST_CASE("Options::parse_command_line(), --new-issue")
{
	std::vector<std::string> args;
	args.push_back("--new-issue");

	Options opt;
	const auto res = opt.parse_command_line(args);

	REQUIRE(res == true);
	REQUIRE(opt.new_issue == true);
}

TEST_CASE("Options::parse_command_line(), --prs")
{
	std::vector<std::string> args;
	args.push_back("--prs");

	Options opt;
	const auto res = opt.parse_command_line(args);

	REQUIRE(res == true);
	REQUIRE(opt.pull_requests == true);
}

TEST_CASE("Options::parse_command_line(), --mrs")
{
	std::vector<std::string> args;
	args.push_back("--mrs");

	Options opt;
	const auto res = opt.parse_command_line(args);

	REQUIRE(res == true);
	REQUIRE(opt.pull_requests == true);
}

TEST_CASE("Options::parse_command_line(), -N")
{
	std::vector<std::string> args;
	args.push_back("-N");

	Options opt;
	const auto res = opt.parse_command_line(args);

	REQUIRE(res == true);
	REQUIRE(opt.new_pr == true);
}

TEST_CASE("Options::parse_command_line(), --new-pr")
{
	std::vector<std::string> args;
	args.push_back("--new-pr");

	Options opt;
	const auto res = opt.parse_command_line(args);

	REQUIRE(res == true);
	REQUIRE(opt.new_pr == true);
}

TEST_CASE("Options::parse_command_line(), --new-mr")
{
	std::vector<std::string> args;
	args.push_back("--new-mr");

	Options opt;
	const auto res = opt.parse_command_line(args);

	REQUIRE(res == true);
	REQUIRE(opt.new_pr == true);
}

TEST_CASE("Options::parse_command_line(), --pipelines")
{
	std::vector<std::string> args;
	args.push_back("--pipelines");

	Options opt;
	const auto res = opt.parse_command_line(args);

	REQUIRE(res == true);
	REQUIRE(opt.pipelines == true);
}

TEST_CASE("Options::parse_command_line(), --actions")
{
	std::vector<std::string> args;
	args.push_back("--actions");

	Options opt;
	const auto res = opt.parse_command_line(args);

	REQUIRE(res == true);
	REQUIRE(opt.actions == true);
}

TEST_CASE("Options::parse_command_line(), -h -i")
{
	std::vector<std::string> args;
	args.push_back("-h");
	args.push_back("-i");

	Options opt;
	const auto res = opt.parse_command_line(args);

	REQUIRE(res == true);
	REQUIRE(opt.help == true);
	REQUIRE(opt.issues == true);
}
