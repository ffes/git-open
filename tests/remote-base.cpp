#include <catch2/catch.hpp>
#include "../remote.h"

TEST_CASE("Remote::url(), unset")
{
	Remote rmt(nullptr);
	REQUIRE(rmt.url() == "");
	REQUIRE(rmt.host == Remote::unknown);
}
