#include <catch2/catch.hpp>
#include "../remote.h"
#include "../options.h"

TEST_CASE("Remote::url(), bitbucket")
{
	Options options;

	Remote rmt(&options);
	rmt.set_from_url("https://bitbucket.org/user/repo.git");

	REQUIRE(rmt.url() == "https://bitbucket.org/user/repo");
	REQUIRE(rmt.host == Remote::bitbucket);
}

TEST_CASE("Remote::url(), bitbucket, issues")
{
	Options options;
	options.issues = true;

	Remote rmt(&options);
	rmt.set_from_url("https://bitbucket.org/user/repo.git");

	REQUIRE(rmt.url() == "https://bitbucket.org/user/repo/issues");
	REQUIRE(rmt.host == Remote::bitbucket);
}

TEST_CASE("Remote::url(), bitbucket, new_issue")
{
	Options options;
	options.new_issue = true;

	Remote rmt(&options);
	rmt.set_from_url("https://bitbucket.org/user/repo.git");

	REQUIRE(rmt.url() == "https://bitbucket.org/user/repo/issues/new");
	REQUIRE(rmt.host == Remote::bitbucket);
}

TEST_CASE("Remote::url(), bitbucket, issue_from_branch")
{
	Options options;
	options.issue_from_branch = true;
	options.branch = "fix-23";

	Remote rmt(&options);
	rmt.set_from_url("https://bitbucket.org/user/repo.git");

	REQUIRE(rmt.url() == "https://bitbucket.org/user/repo/issues/23");
	REQUIRE(rmt.host == Remote::bitbucket);
}

TEST_CASE("Remote::url(), bitbucket, issue_from_branch, invalid branch")
{
	Options options;
	options.issue_from_branch = true;
	options.branch = "some-branch-name";

	Remote rmt(&options);
	rmt.set_from_url("https://bitbucket.org/user/repo.git");

	REQUIRE(rmt.url() == "https://bitbucket.org/user/repo/issues");
	REQUIRE(rmt.host == Remote::bitbucket);
}

TEST_CASE("Remote::url(), bitbucket, pull-requests")
{
	Options options;
	options.pull_requests = true;

	Remote rmt(&options);
	rmt.set_from_url("https://bitbucket.org/user/repo.git");

	REQUIRE(rmt.url() == "https://bitbucket.org/user/repo/pull-requests");
	REQUIRE(rmt.host == Remote::bitbucket);
}

TEST_CASE("Remote::url(), bitbucket, new-pr, branch not set")
{
	Options options;
	options.new_pr = true;
	options.branch = "";

	Remote rmt(&options);
	rmt.set_from_url("https://bitbucket.org/user/repo.git");

	REQUIRE(rmt.url() == "https://bitbucket.org/user/repo/pull-requests/new");
	REQUIRE(rmt.host == Remote::bitbucket);
}

TEST_CASE("Remote::url(), bitbucket, pipelines")
{
	Options options;
	options.pipelines = true;

	Remote rmt(&options);
	rmt.set_from_url("https://bitbucket.org/user/repo.git");

	REQUIRE(rmt.url() == "https://bitbucket.org/user/repo/addons/pipelines/home");
	REQUIRE(rmt.host == Remote::bitbucket);
}

TEST_CASE("Remote::url(), bitbucket, actions")
{
	Options options;
	options.actions = true;

	Remote rmt(&options);
	rmt.set_from_url("https://bitbucket.org/user/repo.git");

	REQUIRE(rmt.url() == "https://bitbucket.org/user/repo/addons/pipelines/home");
	REQUIRE(rmt.host == Remote::bitbucket);
}
