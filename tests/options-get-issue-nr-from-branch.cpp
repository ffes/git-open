#include <catch2/catch.hpp>
#include <string>
#include "../options.h"

TEST_CASE("Options::get_issue_nr_from_branch(), 69-some-description")
{
	Options opt;
	opt.branch = "69-some-description";
	opt.issue_from_branch = true;

	const auto res = opt.get_issue_nr_from_branch();

	REQUIRE(res == 69);
}

TEST_CASE("Options::get_issue_nr_from_branch(), fix-69-some-description")
{
	Options opt;
	opt.branch = "fix-69-some-description";
	opt.issue_from_branch = true;

	const auto res = opt.get_issue_nr_from_branch();

	REQUIRE(res == 69);
}

TEST_CASE("Options::get_issue_nr_from_branch(), some-description")
{
	Options opt;
	opt.branch = "some-description";
	opt.issue_from_branch = true;

	const auto res = opt.get_issue_nr_from_branch();

	REQUIRE(res == 0);
}

TEST_CASE("Options::get_issue_nr_from_branch(), no branch")
{
	Options opt;
	opt.issue_from_branch = true;

	const auto res = opt.get_issue_nr_from_branch();

	REQUIRE(res == 0);
}

TEST_CASE("Options::get_issue_nr_from_branch(), issue_from_branch = false")
{
	Options opt;
	opt.branch = "some-description";
	opt.issue_from_branch = false;

	const auto res = opt.get_issue_nr_from_branch();

	REQUIRE(res == 0);
}
