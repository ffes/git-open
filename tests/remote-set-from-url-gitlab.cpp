#include <catch2/catch.hpp>
#include "../remote.h"

TEST_CASE("Remote::set_from_url(), gitlab.com, https")
{
	Remote rmt(nullptr);
	const auto res = rmt.set_from_url("https://gitlab.com/user/repo.git");
	REQUIRE(res == true);
	REQUIRE(rmt.url() == "https://gitlab.com/user/repo");
	REQUIRE(rmt.host == Remote::gitlab);
}

TEST_CASE("Remote::set_from_url(), gitlab.com, https with username")
{
	Remote rmt(nullptr);
	const auto res = rmt.set_from_url("https://someone@gitlab.com/user/repo.git");
	REQUIRE(res == true);
	REQUIRE(rmt.url() == "https://gitlab.com/user/repo");
	REQUIRE(rmt.host == Remote::gitlab);
}

TEST_CASE("Remote::set_from_url(), gitlab.com, scp-notation")
{
	Remote rmt(nullptr);
	const auto res = rmt.set_from_url("git@gitlab.com:user/repo.git");
	REQUIRE(res == true);
	REQUIRE(rmt.url() == "https://gitlab.com/user/repo");
	REQUIRE(rmt.host == Remote::gitlab);
}

TEST_CASE("Remote::set_from_url(), gitlab self hosted, scp-notation")
{
	Remote rmt(nullptr);
	const auto res = rmt.set_from_url("git@gitlab.example.com:user/repo.git");
	REQUIRE(res == true);
	REQUIRE(rmt.url() == "https://gitlab.example.com/user/repo");
	REQUIRE(rmt.host == Remote::gitlab);
}

TEST_CASE("Remote::set_from_url(), gitlab self hosted, ssh-url")
{
	Remote rmt(nullptr);
	const auto res = rmt.set_from_url("ssh://gitlab.domain.com/user/repo.git");
	REQUIRE(res == true);
	REQUIRE(rmt.url() == "https://gitlab.domain.com/user/repo");
	REQUIRE(rmt.host == Remote::gitlab);
}

TEST_CASE("Remote::set_from_url(), gitlab self hosted, ssh-url with username")
{
	Remote rmt(nullptr);
	const auto res = rmt.set_from_url("ssh://git@gitlab.domain.com/user/repo.git");
	REQUIRE(res == true);
	REQUIRE(rmt.url() == "https://gitlab.domain.com/user/repo");
	REQUIRE(rmt.host == Remote::gitlab);
}

TEST_CASE("Remote::set_from_url(), gitlab self hosted, https, host type already set")
{
	Remote rmt(nullptr);
	rmt.host = Remote::gitlab;
	const auto res = rmt.set_from_url("https://git.domain.com/user/repo.git");
	REQUIRE(res == true);
	REQUIRE(rmt.url() == "https://git.domain.com/user/repo");
	REQUIRE(rmt.host == Remote::gitlab);
}
