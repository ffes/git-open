#include "open-url.h"

#ifdef WIN32
#include <windows.h>
#else
#include <algorithm>
#include <sys/utsname.h>
#endif

using namespace std;

#ifndef WIN32
static std::string get_open_url_helper()
{
	// Get the details about the OS
	utsname buf;
	uname(&buf);

	// First check the sysname
	string sysname = buf.sysname;
	transform(sysname.begin(), sysname.end(), sysname.begin(), ::tolower);

	if (sysname.find("linux") != string::npos)
	{
		// WSL (Windows Subsystem for Linux) can be detected by checking if
		// the member `release` contains the text "Microsoft"
		// https://github.com/Microsoft/WSL/issues/423
		string release = buf.release;
		transform(release.begin(), release.end(), release.begin(), ::tolower);
		if (release.find("microsoft") != string::npos)
		{
			// We are on WSL
			return "powershell.exe Start";
		}

		// We are a normal Linux!
		return "xdg-open";
	}

	if (sysname.find("cygwin") != string::npos)
	{
		// We are on Cygwin
		return "cygstart";
	}

	if (sysname.find("msys") != string::npos)
	{
		// We are on MSYS/MSYS2 (also Git for Windows)
		return "start";
	}

	return "";
}
#endif

/**
 * Open the URL in the default browser of the operating system
 *
 * @param[in] url The URL to open
 * @return On Windows always return true, on other systems return if system() succeeds
 */

bool open_url(std::string url)
{
#ifdef WIN32
	// On WIN32 we need to use the Windows API to start a program
	ShellExecute(nullptr, nullptr, url.c_str(), nullptr, nullptr, SW_SHOW);
	return true;
#else
	// Add the right command per operating system
	string cmd = get_open_url_helper();

	// Add the URL
	cmd += " " + url;

	// Execute the command
	return system(cmd.c_str()) == 0;
#endif
}
