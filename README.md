git-open
========

`git-open` is a command line tool that opens the page of the repository on the Git Hosting Service in your browser.

Read the full documentation at https://git-open.readthedocs.io


Building
--------

**Prerequisite**

You need a recent version of libgit2 installed on you machine, at least v0.26.
If you are on Cygwin, you need to build libgit2 yourself, because at this moment it still has v0.25.

**Building On Ubuntu**

First install all the thing you need to compile.

```bash
apt install build-essential cmake libgit2-dev
```

After that use the default CMake method:

```bash
mkdir build && cd build
cmake ..
make
```

**Building for Git for Windows**

Make sure you have [MinGW-w64](http://mingw-w64.org/) installed from https://www.msys2.org/.
Once the first installation is complete be sure to install the gcc compilers and cmake.

```bash
pacman -Syu
pacman -S mingw-w64-x86_64-{toolchain,cmake,libgit2}
```

After that, use the slightly adjusted default CMake method:

```bash
mkdir build && cd build
cmake -G "MinGW Makefiles" ..
mingw32-make
```


Unit tests
----------

The project contains units test using [Catch2](https://github.com/catchorg/Catch2) and CMake CTest.

To run the tests start `ctest` or `ctest --output-on-failure` from the `build` directory.
