#include <iostream>
#include <git2.h>

#include "options.h"
#include "remote.h"
#include "open-url.h"
#include "version-git.h"

constexpr char PROGRAM_NAME[] = "git open";

/**
 * Print the program name and version
 */

static void show_version()
{
	using namespace std;
	cout << PROGRAM_NAME << " " << VERSION_GIT_STR << endl;

#ifndef NDEBUG
	int major, minor, rev;
	git_libgit2_version(&major, &minor, &rev);
	cout << "Using libgit2 " << major << "." << minor << "." << rev << endl;
#endif
}

/**
 * Print the short help message
 */

static void show_help()
{
	using namespace std;
	cout << "Usage: " << PROGRAM_NAME << endl;
	cout << endl;
	cout << "Open the page of the repository on the Git Hosting Service in your browser." << endl;
	cout << endl;
	cout << "https://gitlab.com/ffes/git-open" << endl;
	cout << endl;
	cout << "Available options are:" << endl;
	cout << "    -h                Display this help message." << endl;
	cout << "        --help        Open the man page." << endl;			// Note that this is handled by the git(1) command
	cout << "        --version     Display the programs version." << endl;
	cout << "        --print       Instead of opening, display the resulting URL." << endl;
	cout << "    -i, --issues      Open the Issues page." << endl;
	cout << "    -I, --issue       Open the Issue page for the first number found in the branch name." << endl;
	cout << "    -n, --new-issue   Open the page to create a new issue." << endl;
	cout << "    -p, --prs         Open the Pull Requests page." << endl;
	cout << "    -m, --mrs         Open the Merge Requests page." << endl;
	cout << "    -N, --new-pr      Open a new Pull Request based on the current branch." << endl;
	cout << "        --new-mr      Open a new Merge Request based on the current branch." << endl;
	cout << "    -l, --pipelines   Open the Pipelines page." << endl;
	cout << "    -a, --actions     Open the Actions page." << endl;
	cout << endl;
}

/**
 *
 */

static std::string get_remote_url(git_repository *repo, Options *options)
{
	using namespace std;

	// Get the list of remotes
	git_strarray remotes = {0};
	int error = git_remote_list(&remotes, repo);

	if (error != 0)
	{
		cerr << "No remotes found" << endl;
		return "";
	}

	// If there is only one remote, use that
	git_remote *git_rmt = nullptr;
	if (remotes.count == 1)
	{
		error = git_remote_lookup(&git_rmt, repo, remotes.strings[0]);
		if (error == 0)
		{
			git_strarray_free(&remotes);
			string url = git_remote_url(git_rmt);
			git_remote_free(git_rmt);
			return url;
		}
	}
	git_strarray_free(&remotes);

	// See if the the remote set in the options is found
	error = git_remote_lookup(&git_rmt, repo, options->remote.c_str());
	if (error == 0)
	{
		string url = git_remote_url(git_rmt);
		git_remote_free(git_rmt);
		return url;
	}
	git_remote_free(git_rmt);

	cerr << "Remote '" << options->remote << "' not found" << endl;
	return "";
}

/**
 * @brief Set the options from the global and repository config
 */

static void set_options_from_git_config(git_repository *repo, Options *options)
{
	// See if the default remote has been set in the config
	git_config *cfg = nullptr;
	int error = git_repository_config_snapshot(&cfg, repo);
	if (error == 0)
	{
		const char* default_remote = nullptr;
		error = git_config_get_string(&default_remote, cfg, "open.default.remote");
		if (error == 0)
		{
			options->remote = default_remote;
		}
	}
	git_config_free(cfg);

	// If the remote is not set, use `origin` as the default value
	if (options->remote.length() == 0)
		options->remote = "origin";
}

/**
 * @brief Get the current branch in the repository and store it in the options
 */

static void get_current_branch(git_repository *repo, Options *options)
{
	git_branch_iterator *it;
	if (!git_branch_iterator_new(&it, repo, GIT_BRANCH_ALL))
	{
		// Go through all the branches
		git_reference *ref;
		git_branch_t type;
		bool found = false;
		while (!found && !git_branch_next(&ref, &type, it))
		{
			// Is the branch checked out?
			if (git_branch_is_checked_out(ref))
			{
				// Get the name of the branch
				const char *branch = nullptr;
				git_branch_name(&branch, ref);

				// Store in the settings
				options->branch = branch;

				// No need to search any further
				found = true;
			}

			git_reference_free(ref);
		}
		git_branch_iterator_free(it);
	}
}

/**
 * The main program
 */

int main(int argc, char *argv[])
{
	using namespace std;

	// Convert command line options to a vector of strings
	vector<string> args;
	for (int i = 1; i < argc; i++)
		args.push_back(argv[i]);

	// Parse the command line options
	Options options;
	if (!options.parse_command_line(args))
	{
		cerr << options.error << endl;
		return 1;
	}

	// Print the help
	if (options.help)
	{
		show_help();
		return 0;
	}

	// Print the version information
	if (options.version)
	{
		show_version();
		return 0;
	}

	// Initialise libgit2
	git_libgit2_init();

	// Open the current repository
	git_repository *repo = nullptr;
	int error = git_repository_open_ext(&repo, ".", 0, nullptr);

	// Anything non-zero is an error. Most likely "not a git repo"
	if (error != 0)
	{
		const git_error *e = giterr_last();
		cerr << "Error: " << e->message << endl;
		return 1;
	}

	// Set various settings from git config
	set_options_from_git_config(repo, &options);

	// Get the url from the remote of the repository
	const string remote_url = get_remote_url(repo, &options);

	// No URL found
	if (remote_url.length() == 0)
		return 1;

	// Get the current branch from the repository
	get_current_branch(repo, &options);

	git_repository_free(repo);

	// Transform the git remote into the proper URL
	Remote rmt(&options);
	if (!rmt.set_from_url(remote_url))
		return 1;

	if (rmt.url().length() == 0)
	{
		cerr << "Unable to transform remote " << remote_url << " to an URL" << endl;
		return 1;
	}

	// Shutdown libgit2 properly
	git_libgit2_shutdown();

	// Do we only need to print the URL?
	if (options.print)
	{
		cout << rmt.url() << endl;
		return 0;
	}

	// Now open the URL
	return open_url(rmt.url()) ? 0 : 1;
}
