#!/usr/bin/env bash

# Is git installed?
if ! type git >/dev/null 2>&1
then
	echo "Git not found, generating dummy version-git.h..."
	echo "#pragma once" > version-git.h
	echo "constexpr char VERSION_GIT_STR[] = \"UNKNOWN VERSION\";" >> version-git.h
	exit 0
fi

# Get the latest git commit
if ! VERSION=$(git describe --tags)
then
	echo "No git tags set yet, generating dummy version-git.h..."
	echo "#pragma once" > version-git.h
	echo "constexpr char VERSION_GIT_STR[] = \"UNKNOWN VERSION\";" >> version-git.h
	exit 0
fi

# Make sure there is a file to grep, but don't touch it if not needed
[[ -f version-git.h ]] || touch version-git.h

# Has the version changed?
if ! grep --quiet $VERSION version-git.h
then
	echo "Generating version-git.h..."
	echo "#pragma once" > version-git.h
	echo "constexpr char VERSION_GIT_STR[] = \"$VERSION\";" >> version-git.h
	exit 0
fi
