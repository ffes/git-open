#include "options.h"

Options::Options()
{
	help = false;
	version = false;
	print = false;
	error.clear();
	remote.clear();
	branch.clear();
	issues = false;
	issue_from_branch = false;
	new_issue = false;
	pull_requests = false;
	new_pr = false;
	pipelines = false;
	actions = false;
}

/**
 *
 */

bool Options::parse_command_line(std::vector<std::string> args)
{
	using namespace std;

	for (int i = 0; i < args.size(); i++)
	{
		string arg = args[i];

		// Print the short help text
		if (arg == "-h")
		{
			help = true;
			continue;
		}

		// Print the program name and the version
		if (arg == "--version")
		{
			version = true;
			continue;
		}

		// Print the resulting URL instead of opening it
		if (arg == "--print")
		{
			print = true;
			continue;
		}

		// Open the Issues page
		if (arg == "-i" || arg == "--issues")
		{
			issues = true;
			continue;
		}

		// Open the Issue page for the first number found in the branch name
		if (arg == "-I" || arg == "--issue")
		{
			issue_from_branch = true;
			continue;
		}

		// Open the page to create a new issue
		if (arg == "-n" || arg == "--new-issue")
		{
			new_issue = true;
			continue;
		}

		// Open the Pull Requests page
		if (arg == "-p" || arg == "--prs")
		{
			pull_requests = true;
			continue;
		}

		// On GitLab they are called Merge Requests
		// Note that --prs on GitLab works and --mrs on GitHub works as well!
		if (arg == "-m" || arg == "--mrs")
		{
			pull_requests = true;
			continue;
		}

		// Open a new PR or MR based on the name of the current branch
		if (arg == "-N" || arg == "--new-pr" || arg == "--new-mr")
		{
			new_pr = true;
			continue;
		}

		// Open the GitHub Actions page
		if (arg == "-a" || arg == "--actions")
		{
			actions = true;
			continue;
		}

		// Open the Pipelines page
		if (arg == "-l" || arg == "--pipelines")
		{
			pipelines = true;
			continue;
		}

		// Option was not recognized
		error = "Invald option: " + arg;
		return false;
	}
	return true;
}

/**
 *
 */

int Options::get_issue_nr_from_branch()
{
	if (branch.length() == 0 || !issue_from_branch)
		return 0;

	std::string numbers = "0123456789";
	std::size_t found = branch.find_first_of(numbers.c_str());

	if (found == std::string::npos)
		return 0;

	return std::stoi(branch.substr(found));
}
