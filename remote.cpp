#include <regex>
#include "remote.h"
#include "options.h"

using namespace std;

Remote::Remote(Options* opts)
{
	host = unknown;
	options = opts;
	clear_url();
}

void Remote::clear_url()
{
	hostname.clear();
	basepath.clear();
}

/**
 * @brief Tranform the git remote URI to the proper web URL of the corresponding git hosting service
 *
 * @param[in] remote_url The `git remote` URL
 * @return On success, returns true
 */

bool Remote::set_from_url(std::string remote_url)
{
	try
	{
		// Clear all the members required to set generate the URL
		clear_url();

		vector<string> regexs;

		// https://host/user/repo.git
		// https://usr@host/user/repo.git
		regexs.push_back("https?:\\/\\/(?:[a-z0-9-_.]+@)?([^\\/]+)\\/([^\\/]+)\\/(.+?)(?:\\.git)?$");

		// git@host:user/repo.git
		regexs.push_back("^git@([^:]+):([^\\/]+)\\/(.+?)(?:\\.git)?$");

		// git://host/user/repo.git
		regexs.push_back("^git:\\/\\/([^\\/]+)\\/([^\\/]+)\\/(.+?)(?:\\.git)?$");

		// ssh://host/user/repo.git
		// ssh://usr@host/user/repo.git
		regexs.push_back("^ssh:\\/\\/(?:[^@]+@)?([^\\/]+)\\/([^\\/]+)\\/(.+?)(?:\\.git)?$");

		// Go through all the regexs
		for (auto r: regexs)
		{
			smatch m;
			if (regex_search(remote_url, m, regex(r, regex::ECMAScript | regex::icase)))
			{
				if (m.size() == 4)
				{
					hostname = m[1];

					basepath = m[2];		// user or group or organization
					basepath += "/";
					basepath += m[3];		// repository

					// If the host type is not set already, try to figure it out
					if (host == unknown)
					{
						if (hostname.find("github") != string::npos)
							host = github;
						else if (hostname.find("gitlab") != string::npos)
							host = gitlab;
						else if (hostname.find("bitbucket") != string::npos)
							host = bitbucket;
					}

					// We're done
					return true;
				}
			}
		}
	}
	catch (regex_error& e)
	{
	}

	return false;
}

/**
 * @brief Generate the URL for the Git Hosting Service
 *
 * @return The URL for the Git Hosting Service
 */

std::string Remote::url()
{
	if (hostname.length() == 0 || basepath.length() == 0)
		return "";

	string url = "https://";
	url += hostname;
	url += "/";
	url += basepath;

	string additional = additional_path();
	if (additional.length() > 0)
	{
		url += "/";
		url += additional;
	}

	return url;
}

/**
 * @brief Get the additional path, based on the options, for the hosting service of the repo.
 *
 * @return The path to add to the repostory's base url.
 *         If no hosting service was detected returns as empty string.
 */

std::string Remote::additional_path()
{
	if (options == nullptr)
		return "";

	// Add the additional paths for the various hosting services
	switch (host)
	{
		case github:
			return additional_path_github();

		case gitlab:
			return additional_path_gitlab();

		case bitbucket:
			return additional_path_bitbucket();
	}

	// Unknown hosting service
	return "";
}

/**
 * @brief Get the additional path, based on the options, for GitHub.
 *
 * @return The path to add to the repostory's base url.
 *         If none of the given options was applicable return an empty string.
 */

std::string Remote::additional_path_github()
{
	if (options->issues)
		return "issues";

	if (options->new_issue)
		return "issues/new";

	if (options->issue_from_branch)
	{
		int issue_nr = options->get_issue_nr_from_branch();
		if (issue_nr == 0)
			return "issues";

		return "issues/" + std::to_string(issue_nr);
	}

	if (options->pull_requests)
		return "pulls";

	if (options->new_pr)
	{
		if (options->branch == "")
			return "compare";
		else
			return "pull/new/" + options->branch;
	}

	if (options->actions || options->pipelines)
		return "actions";

	// No options applicable
	return "";
}

/**
 * @brief Get the additional path, based on the options, for GitLab.
 *
 * @return The path to add to the repostory's base url.
 *         If none of the given options was applicable return an empty string.
 */

std::string Remote::additional_path_gitlab()
{
	if (options->issues)
		return "-/issues";

	if (options->new_issue)
		return "-/issues/new";

	if (options->issue_from_branch)
	{
		int issue_nr = options->get_issue_nr_from_branch();
		if (issue_nr == 0)
			return "-/issues";

		return "-/issues/" + std::to_string(issue_nr);
	}

	if (options->pull_requests)
		return "-/merge_requests";

	if (options->new_pr)
	{
		std::string ret = "-/merge_requests/new";
		if (options->branch != "")
			ret += "?merge_request%5Bsource_branch%5D=" + options->branch;
		return ret;
	}

	if (options->pipelines || options->actions)
		return "-/pipelines";

	// No options applicable
	return "";
}

/**
 * @brief Get the additional path, based on the options, for Bitbucket.
 *
 * @return The path to add to the repostory's base url.
 *         If none of the given options was applicable return an empty string.
 */

std::string Remote::additional_path_bitbucket()
{
	if (options->issues)
		return "issues";

	if (options->new_issue)
		return "issues/new";

	if (options->pull_requests)
		return "pull-requests";

	if (options->issue_from_branch)
	{
		int issue_nr = options->get_issue_nr_from_branch();
		if (issue_nr == 0)
			return "issues";

		return "issues/" + std::to_string(issue_nr);
	}

	if (options->new_pr)
		return "pull-requests/new";

	if (options->pipelines || options->actions)
		return "addons/pipelines/home";

	// No options applicable
	return "";
}
