# v0.6.1 (2022-01-19)

* Return the proper exit code


# v0.6.0 (2020-09-09)

* Add initial support for Bitbucket
* Add more command line options to open subpages like Issues, Pull/Merge Requests
* Add basic documentation


# v0.5.0 (2019-07-06)

* Initial release
