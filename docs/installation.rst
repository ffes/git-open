Installation
============

Basic install on Linux
----------------------

The easiest way of install is to :ref:`build<build>` the tool and put ``git-open`` somewhere into your path, for example by copying it into an existing included path like ``/usr/local/bin``.

To install the man page generate the man page and put ``git-open.1`` it somewhere in the path specified by ``man --path``.
Note that you main need to create a subdirectory ``man1`` and put it in there.
