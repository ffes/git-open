.. _build:

How to build
============

The tool is written in C++ and makes use of `libgit2`_. It uses CMake to bootstrap the build process.

Linux
-----

On Linux you can use the default CMake method:

.. code:: bash

	mkdir build && cd build
	cmake ..
	make

Unit tests
----------

The project contains units test using `Catch2`_ and CMake CTest.

To run the tests start ``ctest`` or ``ctest --output-on-failure`` from the ``build`` directory.

.. _libgit2: https://libgit2.org/
.. _Catch2: https://github.com/catchorg/Catch2
