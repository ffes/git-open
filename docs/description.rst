Description
===========

``git-open`` opens the page of the repository on the Git Hosting Service in your browser.
