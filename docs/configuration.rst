.. _configuration:

Configuration
=============

To configure ``git-open`` you may need to set some ``git config`` options.
You can use ``--global`` to set across all repositories, instead of just the current one.

.. code-block:: bash

	git config [--global] option value

Configuring which remote to open
--------------------------------

If there is only one remote, ``git-open`` always opens that.

Otherwise ``git-open`` opens the remote named ``origin``, but you may want to override this behavior.

When you fork a project and add a remote named ``upstream`` you often want that ``upstream`` to be opened rather than your fork.
To accomplish this, you can set the ``open.default.remote`` within your project:

.. code-block:: bash

	git config open.default.remote upstream
