.. _options:

Options
=======

``--help``
  Open the man page.

``-h``
  Display short help.

``--version``
  Display the programs version.

``--print``
  Instead of opening, display the resulting URL.

``-i``, ``--issues``
  Open the Issues page.

``-n``, ``--new-issue``
  Open the page to create a new issue.

``-I``, ``--issue``
  Open the current issue page.

  When the name of the current branch contains a number, the first number in the branch name is considered to be the number of the issue this branch is related to.
  It will open the page with that issue.
  When no number is found in the branch name it will open the general Issue page.

``-p``, ``--prs``
  Open the Pull Requests page.

``-m``, ``--mrs``
  Open the Merge Requests page.

``-N``, ``--new-pr``, ``--new-mr``
  Open the new Pull Request or new Merge Request page based on the current branch.
  This assumes the current branch has already been pushed to the remote.

``-l``, ``--pipelines``
  Open the Pipelines page on GitLab.
  If the option is used on a GitHub repository, it will open the GitHub Actions page.

``-a``, ``--actions``
  Open the GitHub Actions page.
  If the option is used on a GitLab repository, it will open the GitLab Pipelines page.
