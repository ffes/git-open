.. _Hosting Services:

Supported Git Hosting Services
==============================

``git-open`` can determine the page of the repository for the following git hosting services:

* github.com
* gitlab.com
* Self-hosted GitLab, when its hostname contains ``gitlab``
* bitbucket.org

Know that not all command line options work on every hosting service.
In that case the default page of the repository is opened.
