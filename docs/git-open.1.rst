git-open man page
=================

.. toctree::
	:maxdepth: 2

	synopsis.rst
	description.rst
	options.rst
	configuration.rst
	hosting-services.rst
