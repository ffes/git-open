git-open documentation
======================

This is the documentation of `git-open`, the man page.
For easy reading of this documentation you can also go to https://git-open.readthedocs.io

How to help write docs
----------------------

To help writing the documentation you need to know a bit of reStructuredText.
A good place to start is https://docutils.sourceforge.io/rst.html

A handy reStructuredText and Sphinx cheatsheet can be found at
https://sphinx-tutorial.readthedocs.io/cheatsheet/

Build the docs local
--------------------

To build the documentation local you need to have [Python Sphinx](https://www.sphinx-doc.org) installed and you want to build the HTML-docs you need the theme as well.
To install those Python packages use `pip install sphinx sphinx_rtd_theme`.
There are various ways to install packages locally, like virtualenv, pipenv or Docker.

There is a `Makefile` in the `docs` subdirectory to build various output formats, but `make man` or `make html` is probably what you are looking for.
