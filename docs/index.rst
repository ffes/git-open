.. git-open documentation master file

Welcome to git-open documentation
=================================

.. toctree::
	:maxdepth: 2

	installation.rst
	git-open.1.rst
	build.rst
