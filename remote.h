#pragma once

#include <string>

class Options;

class Remote
{
public:
	enum HostingService { unknown, github, gitlab, bitbucket };

	HostingService host;

	Remote(Options* options);

	bool set_from_url(std::string remote_url);
	std::string url();

private:
	Options* options;
	std::string hostname;
	std::string basepath;

	std::string additional_path();
	std::string additional_path_bitbucket();
	std::string additional_path_gitlab();
	std::string additional_path_github();

	void clear_url();
};
